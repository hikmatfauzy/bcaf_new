﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using nsAlienRFID;
using System.Collections;
using gta.app.desktop.Models;
using System.IO;
using System.Diagnostics;

namespace gta.app.desktop
{
    public partial class Form1 : Form
    {
        private clsReader mReader;
        private ReaderInfo mReaderInfo;
        private ComInterface meReaderInterface = ComInterface.enumTCPIP;
        private ArrayList malTags;
        private String msTags;

        private string BasePath = System.IO.Directory.GetCurrentDirectory();
        private string ExportPath = System.IO.Directory.GetCurrentDirectory() + "\\Export";
        private string DocumentPath = System.IO.Directory.GetCurrentDirectory() + "\\Document";
        public Form1()
        {
            InitializeComponent();
            mReader = new clsReader();
            mReaderInfo = mReader.ReaderSettings;
            ManageGUI(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            try     // extra precausion though it shouldn't throw exceptions
            {
                if (meReaderInterface == ComInterface.enumTCPIP)
                    mReader.InitOnNetwork(tbIPAddress.Text, 23);
                else
                    mReader.InitOnCom();


                lblStatus.Text = "Connecting to the reader...";
                this.Cursor = Cursors.WaitCursor;

                string str = mReader.Connect();
                if (mReader.IsConnected)
                {

                    if (meReaderInterface == ComInterface.enumTCPIP)
                    {
                        lblStatus.Text = "Logging in...";
                        this.Cursor = Cursors.WaitCursor;
                        if (!mReader.Login("alien", "password"))        //returns result synchronously
                        {
                            lblStatus.Text = "Login failed! Calling Disconnect()...";
                            mReader.Disconnect();
                            return;             //------------>
                        }
                        else
                            mReader.AntennaSequence = "0,1,2,3";
                    }
                    ManageGUI(true);
                    //textReaderTalk.Visible = true;

                    // to make it faster and not to lose any tag
                    mReader.AutoMode = "On";
                    mReader.TagListFormat = "Custom";
                    mReader.TagListCustomFormat = "ant: %a; tag: %k; time: %d %t; last: %D %T; rssi: %m";

                }
                lblStatus.Text = str;
            }
            catch (Exception ex)
            {
                lblStatus.Text = "Exception in btnConnect_Click(): " + ex.Message;
            }
            this.Cursor = Cursors.Default;
        }

        private void ManageGUI(bool flag)
        {
            btnDisconnect.Enabled = flag;
            btnConnect.Enabled = !flag;
            //grpConnection.Enabled = !flag;
            btnStartRead.Enabled = flag;
            //btnParse.Enabled = flag;
            //groupFormat.Enabled = flag;
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            mReader.AutoMode = "Off";
            mReader.Disconnect();
            ManageGUI(false);
            this.Cursor = Cursors.Default;
            //if (meReaderInterface == ComInterface.enumTCPIP)
            //    grpNetwork.Enabled = true;
            //else
            //    grpSerial.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            mReader.TagListFormat = "Text";

            string result = null;
            try
            {
                result = mReader.TagList;
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
            }
            if ((result.Length > 0) && (result.IndexOf("No Tags") == -1))
                msTags = result;
            else
                lblStatus.Text = result;
            //DisplayText(result);
            this.Cursor = Cursors.Default;
            //btnParse.Enabled = true;

            if ((msTags != null) && (msTags.Length > 0))
            {
                this.Cursor = Cursors.WaitCursor;
                TagInfo[] aTags;
                try
                {
                    int cnt = AlienUtils.ParseTagList(msTags, out aTags);
                    if (cnt == 0)
                    {
                        lblStatus.Text = "No tags to parse.";
                        btnExport.Enabled = false;
                    }
                    else
                    {
                        malTags = new ArrayList(aTags);
                        List<clsTagInfo> listTag = new List<clsTagInfo>();
                        int i = 1;
                        foreach (TagInfo TagTemp in aTags)
                        {
                            clsTagInfo clsTag = new clsTagInfo();
                            clsTag.RecordTimestamp = "19000001";
                            clsTag.RecordID = i++;
                            clsTag.Protocol = TagTemp.Protocol.ToString().Replace("enum", "");
                            clsTag.Region = "ID";
                            clsTag.Blank = "";
                            clsTag.Tag = TagTemp.TagID.Replace(" ", "");
                            clsTag.Status = "READ";

                            listTag.Add(clsTag);
                        }
                        dg1.DataSource = listTag;
                        btnExport.Enabled = true;
                        if (cnt == 1)
                            lblStatus.Text = "1 tag found.";
                        else
                            lblStatus.Text = cnt.ToString() + " tags found.";
                    }
                }
                catch (Exception ex)
                {
                    // e.g.: in case of malformed XML
                    lblStatus.Text = "Exception parsing tag list " + ex.Message;
                }
                this.Cursor = Cursors.Default;
            }
            //List<clsTagInfo> listTag = new List<clsTagInfo>();
            //clsTagInfo clsTag = new clsTagInfo();
            //clsTag.RecordTimestamp = "19000001";
            //clsTag.RecordID = 1;
            //clsTag.Protocol = "a";
            //clsTag.Region = "ID";
            //clsTag.Blank = "";
            //clsTag.Tag = "test";
            //clsTag.Status = "READ";
            //listTag.Add(clsTag);
            //dg1.DataSource = listTag;

        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                bool exists = Directory.Exists(ExportPath);
                if (!exists)
                    Directory.CreateDirectory(ExportPath);
                string filePath = ExportPath + "\\BCAF Stock Opname_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls";

                Microsoft.Office.Interop.Excel.Application obExcel = new Microsoft.Office.Interop.Excel.Application();
                System.Globalization.CultureInfo oldCI = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
                obExcel.Application.Workbooks.Add(Type.Missing);
                System.Threading.Thread.CurrentThread.CurrentCulture = oldCI;

                obExcel.Columns.ColumnWidth = 25;

                for(int i = 1; i<dg1.Columns.Count + 1; i++)
                {
                    //obExcel.Cells[1, i] = dg1.Columns[i - 1].HeaderText;
                    obExcel.Cells[1, i] = "";
                }

                for (int i = 0; i < dg1.Rows.Count; i++)
                {
                    for (int j = 0; j< dg1.Columns.Count; j++)
                    {
                        //if(dg1.Rows[i].Cells[j].Value != null)
                        //{
                            obExcel.Cells[i + 2, j + 1] = dg1.Rows[i].Cells[j].Value.ToString();
                        //}
                    }
                }

                obExcel.ActiveWorkbook.SaveCopyAs(filePath);
                obExcel.ActiveWorkbook.Saved = true;
                MessageBox.Show("Export data successfully");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnExplore_Click(object sender, EventArgs e)
        {

            bool exists = System.IO.Directory.Exists(ExportPath);
            if (!exists)
                System.IO.Directory.CreateDirectory(ExportPath);

            System.Diagnostics.Process.Start(ExportPath);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            dg1.DataSource = null;
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            Process.Start(DocumentPath + "\\Manual Guide_BCAF-Stock_Opname.pdf");
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(mReader.IsConnected)
                mReader.Disconnect();

            if (mReader != null)
            {
                mReader.Dispose();
            }
        }
    }
}
