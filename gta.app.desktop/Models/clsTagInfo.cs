﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gta.app.desktop.Models
{
    public class clsTagInfo
    {
        //Timestamp Running Number Protocol    Status Region  RFID TAG
        public string RecordTimestamp { get; set; }
        public long RecordID { get; set; }
        public string Protocol { get; set; }
        public string Status { get; set; }
        public string Region { get; set; }
        public string Blank { get; set; }
        public string Tag { get; set; }
    }
}

